
int WINDOW_WIDTH = 640;
int WINDOW_HEIGHT = 480;

float SIDE = 100.0;
float HEIGHT = sqrt(3 * sq(SIDE) / 4.0);
float SLOPE = 1 / sqrt(3);  // slope of a 30-degree line

class Point {
  float x, y;

  Point(float x1, float y1) {
    x = x1; 
    y = y1;
  }
  Point(Point p) {
    x = p.x; 
    y = p.y;
  } 

  String toString() {
    return x + "," + y;
  }
}

class Line {
  float m, b;
  Line(float slope, float yintercept) {
    m = slope;
    b = yintercept;
  }
  Line(Point p1, Point p2) {
    m = slope(p1, p2);
    b = p2.y - m * p2.x;
  }

  Point intercept(Line l2) {
    if (m != l2.m) { // not parallel lines
      Point returnPoint = new Point(0, 0);
      returnPoint.x = b / (l2.m - m);
      returnPoint.y = m * returnPoint.x + b;
      return returnPoint;
    } 
    return null;
  }

  String toString() {
    return "y = " + m + " * x + " + b;
  }

  float xValue(float y) {
    if (m != 0) {
      return (y - b) / m;
    } else {
      return 0;
    }
  }
  float yValue(float x) {
    return m * x + b;
  }
}

Point ORIGIN = new Point(0, 0);

class Segment {
  Point p1, p2;
  Segment(Point p1val, Point p2val) { 
    p1 = p1val; 
    p2 = p2val;
  }
}

void setup() {
  size(WINDOW_WIDTH, WINDOW_HEIGHT);
  translate(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
  scale(1.0, -1.0);

  stroke(128, 128, 255);
  for (int i = 0; i < WINDOW_WIDTH / 2; i += HEIGHT) {
    line(i, WINDOW_HEIGHT / 2, i, -1 * WINDOW_HEIGHT / 2);
    if (i > 0) {
      line(-1 * i, WINDOW_HEIGHT / 2, -1 * i, -1 * WINDOW_HEIGHT / 2);
    }
  }

  pushMatrix();
  rotate(PI / 3.0);
  for (int i = 0; i < WINDOW_WIDTH; i += HEIGHT) {
    line(i, WINDOW_HEIGHT, i, -1 * WINDOW_HEIGHT);
    if (i > 0) {
      line(-1 * i, WINDOW_HEIGHT, -1 * i, -1 * WINDOW_HEIGHT);
    }
  }
  popMatrix();
  pushMatrix();
  rotate(-1.0 * PI / 3.0);
  for (int i = 0; i < WINDOW_WIDTH; i += HEIGHT) {
    line(i, WINDOW_HEIGHT, i, -1 * WINDOW_HEIGHT);
    if (i > 0) {
      line(-1 * i, WINDOW_HEIGHT, -1 * i, -1 * WINDOW_HEIGHT);
    }
  }
  popMatrix();

  stroke(255, 255, 255);
  line(0, WINDOW_HEIGHT / 2, 0, -1 * WINDOW_HEIGHT / 2);
  line(WINDOW_WIDTH / 2, 0, -1 * WINDOW_WIDTH / 2, 0);
}

float slope(Point p1, Point p2) {
  if (p1.x == p2.x) {
    return 0;
  } else if (p1.x == p2.x) {
    return 9999999;
  } else {
    return (p2.y - p1.y) / (p2.x - p1.x);
  }
}

Point mousePointToGraph(Point p) {
  return new Point(p.x - WINDOW_WIDTH / 2, (WINDOW_HEIGHT - p.y) - WINDOW_HEIGHT / 2);
}

Point graphToMousePoint(Point p) {
  return new Point(p.x + WINDOW_WIDTH / 2, WINDOW_HEIGHT - (p.y + WINDOW_HEIGHT / 2));
}

boolean pointIsInTriangle(Point p) {
  return (
  abs(p.x) <= HEIGHT && abs(p.y) <= SIDE / 2 &&
    ( 
  (p.x == 0 && p.y == 0) ||
    (abs(slope(ORIGIN, p)) <= SLOPE)
    )
    );
}

Point delta(Point p) { // figure out the delta, if any, that needs to be applied to p to get it in a triangle

  float addX = 0.0; 
  float addY = 0.0;

  if (! pointIsInTriangle(new Point(p.x + addX, p.y + addY))) {
    while (p.x + addX > HEIGHT) {
      addX -= HEIGHT * 2.0;
    }
    while (p.x + addX < -1 * HEIGHT) {
      addX += HEIGHT * 2.0;
    }
    while (p.y + addY > SIDE / 2.0) {
      addY -= SIDE;
    }
    while (p.y + addY < -1 * SIDE / 2.0) {
      addY += SIDE;
    }
    if (p.x + addX > 0) {
      if (p.y + addY > 0) {
        if (slope(ORIGIN, new Point(p.x + addX, p.y + addY)) > SLOPE) {
          addX -= HEIGHT;
          addY -= SIDE / 2.0;
        }
      } else {
        if (abs(slope(ORIGIN, new Point(p.x + addX, p.y + addY))) > SLOPE) {
          addX -= HEIGHT;
          addY += SIDE / 2.0;
        }
      }
    } else {
      if (p.y + addY > 0) {
        if (abs(slope(ORIGIN, new Point(p.x + addX, p.y + addY))) > SLOPE) {
          addX += HEIGHT;
          addY -= SIDE / 2.0;
        }
      } else {
        if (abs(slope(ORIGIN, new Point(p.x + addX, p.y + addY))) > SLOPE) {
          addX += HEIGHT;
          addY += SIDE / 2.0;
        }
      }
    }
  }
  return new Point(addX, addY);
}

Line diag1 = new Line(SLOPE, 0);
Line diag2 = new Line(-1 * SLOPE, 0);

ArrayList<Point> triangleize(Point p1, Point p2) {
  /* The plan:
   
   1) if p1 does not lie within the triangles, translate p1 and apply that delta to p2 also.
   2) for line segment p1-p2, determine if there is a point I within that line segment that intersects a diagonal.
   3) if not, then the entire segment lies inside a triangle, so push p1 & p2 on the list of segments and we're done
   4) push p1 & I on a list of segments
   5) re-translate I appropriately (as I'), and apply that delta to p2
   6) set p1 equal to I'
   7) go to 2 */

  ArrayList<Point> returnArray = new ArrayList<Point> ();

  // step 1
  if (! pointIsInTriangle(p1)) {
    Point delta = delta(p1);
    p1.x += delta.x; 
    p2.x += delta.x;
    p1.y += delta.y; 
    p2.y += delta.y;
  }

  // while (true)
  for (int iter = 0; iter < 10; ++iter) 
  { // the loop between steps 2 to 7
    ArrayList<Point> potentials = new ArrayList<Point> ();
    Line l = new Line(p1, p2);
    potentials.add(l.intercept(diag1));
    potentials.add(l.intercept(diag2));
    // also have to confirm that the line segment does not exceed the x boundaries
    potentials.add(new Point(HEIGHT, l.yValue(HEIGHT)));
    potentials.add(new Point(-1 * HEIGHT, l.yValue(-1 * HEIGHT)));

    // step 2
    Point I = null;
    for (int i=0; i < potentials.size (); ++i ) {
      if (I == null && potentials.get(i).x >= min(p1.x, p2.x) &&potentials.get(i).x <= max(p1.x, p2.x) && abs(p1.x - potentials.get(i).x) > 0.0001) {
        I = potentials.get(i);
      } else if (I != null && abs(p1.x - I.x) > abs(p1.x - potentials.get(i).x) && potentials.get(i).x >= min(p1.x, p2.x) && potentials.get(i).x <= max(p1.x, p2.x) && abs(p1.x - potentials.get(i).x) > 0.0001) {
        I = potentials.get(i);
      }
    }

    // step 3
    if (I == null) {
      returnArray.add(new Point(p1));
      returnArray.add(new Point(p2));
      return returnArray;
    } else { // there is an intercept
      // step 4

      returnArray.add(new Point(p1));
      returnArray.add(new Point(I));

      // step 5
      if (I.x == HEIGHT) { // special case
        I.x -= 2 * HEIGHT;
        p2.x -= 2 * HEIGHT;
      } else if (I.x == -1 * HEIGHT) {
        I.x += 2 * HEIGHT;
        p2.x += 2 * HEIGHT;
      } else if (I.x > 0) {
        I.x -= HEIGHT;
        p2.x -= HEIGHT;
        if (I.y > 0) {
          I.y -= SIDE / 2.0;
          p2.y -= SIDE / 2.0;
        } else {
          I.y += SIDE / 2.0;
          p2.y += SIDE / 2.0;
        }
      } else {
        I.x += HEIGHT;
        p2.x += HEIGHT;
        if (I.y > 0) {
          I.y -= SIDE / 2.0;
          p2.y -= SIDE / 2.0;
        } else {
          I.y += SIDE / 2.0;
          p2.y += SIDE / 2.0;
        }
      }

      // step 6
      p1 = I;
    }
  }
  return null;
}

ArrayList<Segment> savedSegs = new ArrayList<Segment> ();

void drawAllSortsOfLines(Point p1, Point p2) {
  Point cp1 = mousePointToGraph(p1), cp2 = mousePointToGraph(p2);
  ArrayList<Point> drawPoints = triangleize(cp1, cp2);
  if (drawPoints != null) {
    for (int tessX = -10; tessX < 10; ++tessX) {
      for (int tessY = -10; tessY < 10; ++tessY) {
        for (int i = 0; i < drawPoints.size (); i += 2) {
          for (int sign = -1; sign <= 1; sign += 2) {
            Point start1 = new Point(drawPoints.get(i));
            Point start2 = new Point(drawPoints.get(i+1));

            start1.x *= sign;
            start2.x *= sign; 

            start1.x += tessX * HEIGHT * 2.0;
            start1.y += tessY * SIDE; 
            start2.x += tessX * HEIGHT * 2.0;
            start2.y += tessY * SIDE; 

            if (tessX == 0 && tessY == 0) {
              savedSegs.add(new Segment(new Point(start1), new Point(start2)));
            }

            Point draw1 = graphToMousePoint(start1);
            Point draw2 = graphToMousePoint(start2);
            line(draw1.x, draw1.y, draw2.x, draw2.y);

            start1.x += HEIGHT;
            start1.y += SIDE / 2.0;
            start2.x += HEIGHT;
            start2.y += SIDE / 2.0;

            draw1 = graphToMousePoint(start1);
            draw2 = graphToMousePoint(start2);
            line(draw1.x, draw1.y, draw2.x, draw2.y);
          }
        }
      }
    }
  }
}

int mode = 0; // 0 = line mode, 1 = draw mode
Point lastPoint = new Point(0, 0);
void draw() {
  if (mousePressed == true && mode == 1) {
    drawAllSortsOfLines(lastPoint, new Point(mouseX, mouseY));
  }
  lastPoint.x = mouseX;
  lastPoint.y = mouseY;
  textFont(loadFont("CourierNewPS-BoldMT-48.vlw"), 16);
  fill(0, 0, 255);
  rect(0, WINDOW_HEIGHT - 18, WINDOW_WIDTH, WINDOW_HEIGHT);
  fill(255, 255, 255);
  if (mode == 0) {
    text(" F1=draw mode  F2=reset  F3=save", 1, WINDOW_HEIGHT - 4);
  } else {
    text(" F1=line mode  F2=reset  F3=save", 1, WINDOW_HEIGHT - 4);
  }
}

String saveDir = null;

void exportSvg() {
  try {
    String outfile = saveDir + "/out.svg";
    OutputStream os = createOutput(outfile);

    os.write("<svg xmlns=\"http://www.w3.org/2000/svg\">".getBytes());
    String startG = "<g transform = \"translate(" + (WINDOW_WIDTH / 2) + "," + (WINDOW_HEIGHT / 2) + ")\">";
    os.write(startG.getBytes()); 
    for (int i = 0; i < savedSegs.size (); ++i) {
      String s = "<line x1=\"" + savedSegs.get(i).p1.x + "\" y1=\"" + str(-1 * savedSegs.get(i).p1.y) + "\" stroke=\"black\" x2=\"" + savedSegs.get(i).p2.x + "\" y2=\"" + str(-1 * savedSegs.get(i).p2.y) + "\" />";
      os.write(s.getBytes());
    }

    String buffer;
    buffer = "<line x1=\"" + str(HEIGHT) + "\" y1=\"" + str(SIDE / 2) + "\" stroke=\"blue\" x2=\"" + str(HEIGHT) + "\" y2=\"" + str(SIDE / -2) + "\" /> ";
    os.write(buffer.getBytes());
    buffer = "<line x1=\"" + str(HEIGHT) + "\" y1=\"" + str(SIDE / -2) + "\" stroke=\"blue\" x2=\"" + str(-1 * HEIGHT) + "\" y2=\"" + str(SIDE / 2) + "\" /> ";
    os.write(buffer.getBytes());
    buffer = "<line x1=\"" + str(-1 * HEIGHT) + "\" y1=\"" + str(SIDE / 2) + "\" stroke=\"blue\" x2=\"" + str(-1 * HEIGHT) + "\" y2=\"" + str(SIDE / -2) + "\" /> ";
    os.write(buffer.getBytes());
    buffer = "<line x1=\"" + str(-1 * HEIGHT) + "\" y1=\"" + str(SIDE / -2) + "\" stroke=\"blue\" x2=\"" + str(HEIGHT) + "\" y2=\"" + str(SIDE / 2) + "\" /> ";
    os.write(buffer.getBytes());

    os.write("</g> </svg>".getBytes());
    os.close();
  } 
  catch (IOException e) {
    e.printStackTrace();
  }
}

void saveFile(File selection) {
  if (selection != null) {
    saveDir = selection.getAbsolutePath();
    exportSvg();
  }
}

void keyPressed() {
  if (keyCode == 112) { // F1 - change mode
    mode = 1 - mode;
  } else if (keyCode == 113) { // F2 - reset
    savedSegs.clear();
    fill(205);
    rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    setup();
  } else if (keyCode == 114) { // F3 - save
    if (saveDir == null) {
      selectFolder("Select your save folder", "saveFile");
    } else {
      exportSvg();
    }
  }
}

Point p1 = new Point(0, 0), p2 = new Point(0, 0);
int status = 0;
void mouseClicked() {
  if (mode == 0) {
    if (status == 0) {
      p1.x = mouseX; 
      p1.y = mouseY;
      status = 1;
    } else {
      p2.x = mouseX; 
      p2.y = mouseY;
      drawAllSortsOfLines(p1, p2);
      status = 0;
    }
  }
}

